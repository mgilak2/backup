<?php

namespace Blogator\Components\Backup;

use Blogator\Components\Backup\Contracts\NoSchemaDatabase;

class NoSchemaRepositoryBackup
{
    protected $data = false;
    protected $collections;
    /**
     * @var NoSchemaDatabase
     */
    private $database;

    public function __construct(NoSchemaDatabase $database)
    {
        $this->database = $database;
    }

    public function takeBackup()
    {
        return $this->database->backup();
    }


    public function withData()
    {
        $this->data = true;
    }

    public function collections(array $collections)
    {
        $this->collections = $collections;
    }

    public function take()
    {
        $this->database->setCollections($this->collections);
        return $this->database->backup();
    }
}
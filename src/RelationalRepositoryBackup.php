<?php

namespace Blogator\Components\Backup;

use Blogator\Components\Backup\Contracts\RelationalDatabase;

class RelationalRepositoryBackup
{
    protected $tables;
    protected $where;
    /**
     * @var RelationalDatabase
     */
    private $database;

    public function __construct(RelationalDatabase $database)
    {
        $this->database = $database;
    }

    public function put($where)
    {
        $this->where = $where;
        return $this;
    }

    public function withData()
    {
        $this->database->withData();
        return $this;
    }

    public function withNoData()
    {
        $this->database->withNoData();
        return $this;
    }

    public function tables(array $tables)
    {
        $this->database->tables($tables);
        return $this;
    }

    public function excludeTables(array $tables)
    {
        $this->database->excludeTables($tables);
        return $this;
    }

    public function onlySchema()
    {
        return $this->database->schemaCommand($this->where);
    }

    public function take()
    {
        return $this->database->backupCommand($this->where);
    }

    public function restore($from)
    {
        return $this->database->restoreBackupCommand($from);
    }
}
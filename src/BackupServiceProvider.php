<?php

namespace Blogator\Components\Backup;

use Blogator\Components\Backup\Databases\MySQLCommands;
use Blogator\Components\Backup\Databases\SQLite;
use Blogator\Components\Contracts\BlogatorComponent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class BackUpServiceProvider extends ServiceProvider implements BlogatorComponent
{
    public function describe()
    {
        return [
            "name" => "Backup",
            "priority" => "high",
            "initial" => true,
            "version" => "0.9.0",
        ];
    }

    public function register()
    {
        $this->app->singleton('Blogator\Components\Backup\RepositoryBackup',
            function ($app) {
                return new RelationalRepositoryBackup();
            });

        $this->app->singleton('Blogator\Components\Backup\FilesBackup',
            function ($app) {
                return new FilesBackup();
            });

        $this->app->singleton('Blogator\Components\Backup\Databases\MySQL',
            function ($app) {
                return new MySQLCommands(new ConnectionConfigImpl(DB::connection()->getPdo()));
            });

        $this->app->singleton('Blogator\Components\Backup\Databases\SQLite',
            function ($app) {
                return new SQLite(new ConnectionConfigImpl(DB::connection()->getPdo()));
            });
    }
}
<?php

namespace Blogator\Components\Backup\Settings;

use Blogator\Components\Settings\Contracts\CanHaveSettings;

class BackupSettings implements CanHaveSettings
{
}
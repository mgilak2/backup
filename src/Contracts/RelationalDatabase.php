<?php

namespace Blogator\Components\Backup\Contracts;

interface RelationalDatabase
{
    public function schemaCommand($where);
    public function backupCommand($where);
    public function restoreBackupCommand($from);
    public function tables(array $tables);
    public function excludeTables(array $tables);
    public function allTables();
    public function withData();
    public function withNoData();
}
<?php

namespace Blogator\Components\Backup\Contracts;

interface NoSchemaDatabase
{
    public function setCollections(array $collections);
    public function backup();
    public function withData();
}
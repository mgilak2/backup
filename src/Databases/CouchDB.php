<?php

namespace Blogator\Components\Backup\Databases;

use Blogator\Components\Backup\Contracts\NoSchemaDatabase;

class CouchDB implements NoSchemaDatabase
{
    public function setCollections(array $collections)
    {
    }

    public function backup()
    {
    }

    public function withData()
    {
    }
}
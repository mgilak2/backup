<?php

namespace Blogator\Components\Backup\Databases;

use BackupManager\Databases\MysqlDatabase;
use Blogator\Components\Backup\Contracts\RelationalDatabase;

class MySQLCommands extends MysqlDatabase implements RelationalDatabase
{
    /**
     * @var
     */
    private $config;
    protected $withData = false;
    protected $tables;
    protected $excludeTables;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function tables(array $tables)
    {
        $sanitizedTables = [];
        foreach($tables as $table)
            $sanitizedTables[] = escapeshellarg($table);

        $this->tables = implode(" ", $sanitizedTables);
        return $this;
    }

    public function excludeTables(array $tables)
    {
        $ignoredTables = [];
        foreach($tables as $table){
            $ignoredTables[] = escapeshellarg("--ignore-table=" . $this->config['database'] . "." . $table);
        }
        $this->excludeTables = implode(" ", $ignoredTables);
        return $this;
    }

    public function allTables()
    {
        $this->tables = "*";
        return $this;
    }

    public function withData()
    {
        $this->withData = true;
        return $this;
    }

    public function withNoData()
    {
        $this->withData = false;
        return $this;
    }

    public function restoreBackupCommand($from)
    {
        $this->setConfig($this->config);
        return $this->getRestoreCommandLine($from);
    }

    public function schemaCommand($outputPath)
    {
        return sprintf('mysqldump -d --host=%s --port=%s --user=%s --password=%s %s > %s',
            escapeshellarg($this->config['host']),
            escapeshellarg($this->config['port']),
            escapeshellarg($this->config['user']),
            escapeshellarg($this->config['pass']),
            escapeshellarg($this->config['database']),
            escapeshellarg($outputPath)
        );
    }

    public function backupCommand($outputPath)
    {
        if(strlen($this->tables) > 0)
            return $this->withSpecificTablesCommand($outputPath);
        if(strlen($this->excludeTables) > 0)
            return $this->withExcludedTablesCommand($outputPath);
        elseif($this->withData)
            return $this->allTablesWithDataCommand($outputPath);
        return $this->schemaCommand($outputPath);
    }

    private function withSpecificTablesCommand($outputPath)
    {
        if($this->withData)
            return $this->withSpecificTablesWithDataCommand($outputPath);
        else
            return $this->withSpecificTablesWithNoDataCommand($outputPath);
    }

    private function withExcludedTablesCommand($outputPath)
    {
        if($this->withData)
            return $this->withExcludedTablesWithDataCommand($outputPath);
        else
            return $this->withExcludedTablesWithNoDataCommand($outputPath);
    }

    private function withSpecificTablesWithDataCommand($outputPath)
    {
        return sprintf('mysqldump --host=%s --port=%s --user=%s --password=%s %s %s > %s',
            escapeshellarg($this->config['host']),
            escapeshellarg($this->config['port']),
            escapeshellarg($this->config['user']),
            escapeshellarg($this->config['pass']),
            escapeshellarg($this->config['database']),
            $this->tables,
            escapeshellarg($outputPath)
        );
    }

    private function withExcludedTablesWithDataCommand($outputPath)
    {
        return sprintf('mysqldump --host=%s --port=%s --user=%s --password=%s %s %s > %s',
            escapeshellarg($this->config['host']),
            escapeshellarg($this->config['port']),
            escapeshellarg($this->config['user']),
            escapeshellarg($this->config['pass']),
            escapeshellarg($this->config['database']),
            $this->excludeTables,
            escapeshellarg($outputPath)
        );
    }

    private function withSpecificTablesWithNoDataCommand($outputPath)
    {
        return sprintf('mysqldump -d --host=%s --port=%s --user=%s --password=%s %s %s > %s',
            escapeshellarg($this->config['host']),
            escapeshellarg($this->config['port']),
            escapeshellarg($this->config['user']),
            escapeshellarg($this->config['pass']),
            escapeshellarg($this->config['database']),
            $this->tables,
            escapeshellarg($outputPath)
        );
    }

    private function withExcludedTablesWithNoDataCommand($outputPath)
    {
        return sprintf('mysqldump -d --host=%s --port=%s --user=%s --password=%s %s %s > %s',
            escapeshellarg($this->config['host']),
            escapeshellarg($this->config['port']),
            escapeshellarg($this->config['user']),
            escapeshellarg($this->config['pass']),
            escapeshellarg($this->config['database']),
            $this->excludeTables,
            escapeshellarg($outputPath)
        );
    }

    private function allTablesWithDataCommand($outputPath)
    {
        $this->setConfig($this->config);
        return $this->getDumpCommandLine($outputPath);
    }
}
<?php

use Blogator\Components\Backup\Databases\MySQLCommands;
use Mockery as m;

class MySQLCommandsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MySQLCommands;
     */
    protected $mysql;

    public function setUp()
    {
        $this->mysql = new MySQLCommands([
            "host" => "localhost",
            "port" => "3306",
            "user" => "root",
            "pass" => "root",
            "database" => "yourDatabase",
        ]);
    }

    public function testShouldGenerateBackupCommandForOnlySchema()
    {
        $this->assertEquals(
            "mysqldump -d --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' > '../storage/backup.sql'",
            $this->mysql->schemaCommand("../storage/backup.sql"));
    }

    public function testShouldGenerateBackupCommandForSchemaAndData()
    {
        $this->assertEquals(
            "mysqldump --routines --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' > '../storage/backup.sql'",
            $this->mysql->withData()->backupCommand("../storage/backup.sql"));
    }

    public function testShouldGenerateBackupCommandsForSpecificTablesWithTheirData()
    {
        $this->assertEquals(
            "mysqldump --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' 'users' 'posts' 'comments' > '../storage/backup.sql'",
            $this->mysql->tables(['users', 'posts', 'comments'])
                ->withData()->backupCommand("../storage/backup.sql"));
    }

    public function testShouldGenerateBackupCommandsForSpecificTablesSchema()
    {
        $this->assertEquals(
            "mysqldump -d --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' 'users' 'posts' 'comments' > '../storage/backup.sql'",
            $this->mysql->tables(['users', 'posts', 'comments'])
                ->withNoData()->backupCommand("../storage/backup.sql"));

    }

    public function testShouldGenerateBackupCommandForExcludedTablesWithOtherTablesData()
    {
        $this->assertEquals(
            "mysqldump --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' '--ignore-table=yourDatabase.users' '--ignore-table=yourDatabase.posts' '--ignore-table=yourDatabase.comments' > '../storage/backup.sql'",
            $this->mysql->excludeTables(['users', 'posts', 'comments'])
                ->withData()->backupCommand("../storage/backup.sql"));
    }

    public function testShouldGenerateBackupCommandForExcludedTablesWithNoData()
    {
        $this->assertEquals(
            "mysqldump -d --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' '--ignore-table=yourDatabase.users' '--ignore-table=yourDatabase.posts' '--ignore-table=yourDatabase.comments' > '../storage/backup.sql'",
            $this->mysql->excludeTables(['users', 'posts', 'comments'])
                ->withNoData()->backupCommand("../storage/backup.sql"));
    }

    public function testShouldGenerateRestoreBackupCommand()
    {
        $this->assertEquals(
            "mysql --host='localhost' --port='3306' --user='root' --password='root' 'yourDatabase' -e \"source backup.sql\""
            ,$this->mysql->restoreBackupCommand("backup.sql"));
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}
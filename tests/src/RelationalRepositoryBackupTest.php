<?php

use Mockery as m;

use Blogator\Components\Backup\RelationalRepositoryBackup;
use Blogator\Components\Backup\Contracts\RelationalDatabase;

class RelationalRepositoryBackupTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var RelationalRepositoryBackup
     */
    protected $repository;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockRelationalRepo;

    public function setUp()
    {
        $this->mockRelationalRepo = m::mock(RelationalDatabase::class);
        $this->repository = new RelationalRepositoryBackup($this->mockRelationalRepo);
    }

    public function testOnlySchemaShouldReturnSchemaInSQLString()
    {
        $this->mockRelationalRepo->shouldReceive("schemaCommand")->once()->andReturn("shell command");
        $this->repository->put("../storage/backup.sql")->onlySchema();
    }

    public function testTablesShouldSetTables()
    {
        $this->mockRelationalRepo->shouldReceive("tables")->once();
        $this->repository->tables(['users', 'posts']);
    }

    public function testTakeShouldReturnSqlString()
    {
        $this->mockRelationalRepo->shouldReceive("backupCommand")->once()->andReturn("some shell command");
        $this->repository->put("../storage/backup.sql")->take();
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}
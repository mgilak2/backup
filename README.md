# Phoneme Backup Component
right now we only support mysql. I am working on MongoDB these days
I`m trying to make Eloquent like syntax on code for backups ;)


notice : this repo is under development :) 

### Installation
```
composer require gilak/backup
```

### Usage

taking backup
```php
use BackupManager\ShellProcessing\ShellProcessor;
use Blogator\Components\Backup\Databases\MySQLCommands;
use Blogator\Components\Backup\RelationalRepositoryBackup as Repo;
use Symfony\Component\Process\Process;

$repo = new Repo(new MySQLCommands([
    "host" => "localhost",
    "port" => "3306",
    "user" => "root",
    "pass" => "root",
    "database" => "BackupDatabase"
]));

$shell = new ShellProcessor(new Process(''));
$shell->process($repo->put("backup.sql")->onlySchema());
$shell->process($repo->put("backup.sql")->withData()->take());
$shell->process($repo->put("backup.sql")->withNoData()->take());
$shell->process($repo->put("backup.sql")->withData()->tables(['posts'])->take());
$shell->process($repo->put("backup.sql")->withNoData()->tables(['posts'])->take());
$shell->process($repo->put("backup.sql")->withData()->tables(['posts', 'users'])->take());
$shell->process($repo->put("backup.sql")->withNoData()->tables(['posts', 'users'])->take());
$shell->process($repo->put("backup.sql")->withData()->excludeTables(['posts'])->take());
$shell->process($repo->put("backup.sql")->withNoData()->excludeTables(['posts'])->take());
$shell->process($repo->put("backup.sql")->withNoData()->excludeTables(['posts', 'users'])->take());
$shell->process($repo->put("backup.sql")->withData()->excludeTables(['posts', 'users'])->take());
```

restore backup
```php
$repo = new Repo(new MySQLCommands([
    "host" => "localhost",
    "port" => "3306",
    "user" => "root",
    "pass" => "root",
    "database" => "BackupDatabase"
]));

$shell = new ShellProcessor(new Process(''));

$shell->process($repo->restore("backup.sql"));
```

see examples and tests folders 

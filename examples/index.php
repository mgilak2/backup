<?php

use BackupManager\ShellProcessing\ShellProcessor;
use Blogator\Components\Backup\Databases\MySQLCommands;
use Blogator\Components\Backup\RelationalRepositoryBackup as Repo;
use Symfony\Component\Process\Process;

include __DIR__ . "/../vendor/autoload.php";

//$repo = new Repo(new MySQLCommands([
//    "host" => "localhost",
//    "port" => "3306",
//    "user" => "root",
//    "pass" => "root",
//    "database" => "BackupDatabase"
//]));
//
//$shell = new ShellProcessor(new Process(''));
//$shell->process($repo->put("backup.sql")->onlySchema());
//$shell->process($repo->put("backup.sql")->withData()->take());
//$shell->process($repo->put("backup.sql")->withNoData()->take());
//$shell->process($repo->put("backup.sql")->withData()->tables(['posts'])->take());
//$shell->process($repo->put("backup.sql")->withNoData()->tables(['posts'])->take());
//$shell->process($repo->put("backup.sql")->withData()->tables(['posts', 'users'])->take());
//$shell->process($repo->put("backup.sql")->withNoData()->tables(['posts', 'users'])->take());
//$shell->process($repo->put("backup.sql")->withData()->excludeTables(['posts'])->take());
//$shell->process($repo->put("backup.sql")->withNoData()->excludeTables(['posts'])->take());
//$shell->process($repo->put("backup.sql")->withNoData()->excludeTables(['posts', 'users'])->take());
//$shell->process($repo->put("backup.sql")->withData()->excludeTables(['posts', 'users'])->take());


$repo = new Repo(new MySQLCommands([
    "host" => "localhost",
    "port" => "3306",
    "user" => "root",
    "pass" => "root",
    "database" => "BackupDatabase"
]));

$shell = new ShellProcessor(new Process(''));

$shell->process($repo->restore("backup.sql"));
